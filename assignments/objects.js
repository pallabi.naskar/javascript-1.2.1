const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' }; // use this object to test your functions

// Complete the following underscore functions.
// Reference http://underscorejs.org/ for examples.

//function keys(obj) {
// Retrieve all the names of the object's properties.
// Return the keys as strings in an array.
// Based on http://underscorejs.org/#keys
//}

/**
 * Function to return an array containing keys of the object
 * 
 * @param {*} obj given object
 */
function keys(obj) {
  const myarr = [];
  for (var key in obj) {
    myarr.push(key)
  }
  return myarr
}
console.log(keys(testObject))

//function values(obj) {
// Return all of the values of the object's own properties.
// Ignore functions
// http://underscorejs.org/#values
//}

/**
 * Function to return an array containing values of the object
 * 
 * @param {*} obj given object
 */
function values(obj) {
  const myarr = [];
  for (var key in obj) {
    myarr.push(obj[key])
  }
  return myarr
}
console.log(values(testObject))

//function mapObject(obj, cb) {
// Like map for arrays, but for objects. Transform the value of each property in turn by passing it to the callback function.
// http://underscorejs.org/#mapObject
//}

/**
 * A callback function to transform key to new key
 * 
 * @param {*} key 
 * @param {*} key_val 
 */
const cb = function (key, key_val) {
  return key_val + "New";
}

/**
 * Implements map object function
 * 
 * @param {*} obj object
 * @param {*} cb a callback function
 */
function mapObject(obj, cb) {

  for (var key in obj) {
    obj[key] = cb(key, obj[key])
  }
  return obj
}
console.log(mapObject(testObject, cb))

//function pairs(obj) {
// Convert an object into a list of [key, value] pairs.
// http://underscorejs.org/#pairs
//}

/**
 * Implements pair function to convert an object into a list of [key, value] pairs.
 * 
 * @param {*} obj object
 */
function pairs(obj) {
  const myarr = [];
  for (var key in obj) {
    let subarr = []
    subarr.push(key, obj[key])
    myarr.push(subarr)
  }
  return myarr
}
console.log(pairs(testObject))

/* STRETCH PROBLEMS */

//function invert(obj) {
// Returns a copy of the object where the keys have become the values and the values the keys.
// Assume that all of the object's values will be unique and string serializable.
// http://underscorejs.org/#invert
//}

/**
 * Implements invert function that returns a copy of the object where the keys have become the values and the values the keys
 * 
 * @param {*} obj Object
 */
function invert(obj) {
  var myObj = {};
  for (var key in obj) {
    myObj[obj[key]] = key;
  }
  return myObj
}
console.log(invert(testObject))

//function defaults(obj, defaultProps) {
// Fill in undefined properties that match properties on the `defaultProps` parameter object.
// Return `obj`.
// http://underscorejs.org/#defaults
//}

/**
 * Implements defaults to fill in undefined properties that match properties on the `defaultProps` parameter object
 * 
 * @param {*} obj Object
 * @param {*} defaultProps default properties
 */
function defaults(obj, defaultProps) {
  for (i in defaultProps) {
    if (obj[i] == undefined) {
      obj[i] = defaultProps[i];
    }
  }
  return obj;
}

console.log(defaults(testObject, { name: "pallabi", roll: 28, place: "usa", age: 25 }))
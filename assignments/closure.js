//function counterFactory() {
// Return an object that has two methods called `increment` and `decrement`.
// `increment` should increment a counter variable in closure scope and return it.
// `decrement` should decrement the counter variable and return it.
//}

/**
 * Implements counter factory containing increment/decrement
 */
function counterFactory() {
  var count = 0;
  function increment() { return ++count };
  function decrement() { return --count };

  return { increment, decrement };
}
console.log(counterFactory().increment())

//function limitFunctionCallCount(cb, n) {
// Should return a function that invokes `cb`.
// The returned function should only allow `cb` to be invoked `n` times.
// Returning null is acceptable if cb can't be returned
//}

/**
 * A callback function
 */
const cb = function () { console.log("invoked in cb") };

/**
 * Implements limit function call count method
 * 
 * @param {*} cb callback function
 * @param {*} n number of iteration allowed to invoke the callback function
 */
function limitFunctionCallCount(cb, n) {
  var count = n;
  function invoke() {
    if (count > 0) {
      count--;
      cb();
    }
  }
  return { invoke };
}
var a = limitFunctionCallCount(cb, 3);
a.invoke()
a.invoke()
a.invoke()
a.invoke()
a.invoke()

//function cacheFunction(cb) {
// Should return a funciton that invokes `cb`.
// A cache (object) should be kept in closure scope.
// The cache should keep track of all arguments have been used to invoke this function.
// If the returned function is invoked with arguments that it has already seen
// then it should return the cached result and not invoke `cb` again.
// `cb` should only ever be invoked once for a given set of arguments.
//}

/**
 * The return function of cacheFunction can take various number of arguments and check the argument set in the cache object.
 * If the argument set has been given previously it returns cache value, otheswise it calls callback function
 * 
 * @param {*} cb callback function
 */
function cacheFunction(cb) {
  let cache = {};
  return function (...arr) {
    let myarr = [...arr]
    let key = ``
    for (let i = 0; i < myarr.length; i++) {
      key += `${myarr[i]}_`
    }
    if (cache[key] !== undefined) {
      console.log("same")
      return cache[key];
    }
    else {
      cache[key] = cb(myarr);
      return cache[key];
    }
  }

}
//multipication
const multi = cacheFunction((array) => {
  let mul = 1;
  for (let i = 0; i < array.length; i++) {
    mul *= array[i];
  }
  return mul;
});
//addition
const add = cacheFunction((array) => {
  let sum = 0;
  for (let i = 0; i < array.length; i++) {
    sum += array[i];
  }
  return sum;
})


console.log(multi(5, 5))
console.log(add(6, 2))
console.log(multi(5, 5, 5))
console.log(add(1, 2, 3, 4, 5))
console.log(multi(5, 5, 5))
console.log(add(6, 2));
console.log(multi(5, 5, 5))
console.log(add(6, 2))
